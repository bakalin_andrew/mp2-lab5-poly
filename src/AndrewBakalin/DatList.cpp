#include "DatList.h"

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink) // ��������� �����
{
	return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink) // �������� �����
{
	delete pLink->pValue;
	delete pLink;
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const // ��������
{
	PTDatLink temp;

	switch (mode)
	{
	case FIRST:
		temp = pFirst;
		break;
	case LAST:
		temp = pLast;
		break;
	default:
		temp = pCurrLink;
	}

	if (temp == nullptr)
		return nullptr;
	else
		return temp->pValue;
}

TDatList::TDatList()
{
	pFirst = pLast = pStop = pPrevLink = pCurrLink = nullptr;
	ListLen = 0;
	Reset();
}

void TDatList::SetCurrentPos(int pos) // ���������� ������� �����
{
	if (pos >= ListLen)
		throw 1;
	else
	{
		Reset();
		for (int i = 0; i < pos; i++)
			GoNext();
	}
}

int TDatList::GetCurrentPos() const // �������� ����� ���. �����
{
	return CurrPos;
}

void TDatList::Reset() // ���������� �� ������ ������
{
	if (IsEmpty())
	{
		pCurrLink = pStop;
		CurrPos = -1;
	}
	else
	{
		pCurrLink = pFirst;
		CurrPos = 0;
	}
}

int TDatList::IsListEnded() const // ������ �������� ?
{
	return pCurrLink == pStop;
}

void TDatList::GoNext() // ����� ������ �������� �����
{
	if (pCurrLink != pStop)
	{
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
	}
}

// ������� �������

void TDatList::InsFirst(PTDatValue pVal) // ����� ������
{
	PTDatLink temp = GetLink(pVal, pFirst);

	pFirst = temp;
	ListLen++;

	if (ListLen == 1)
	{
		pLast = temp;
		Reset();
	}

	if (CurrPos == 0)
		pPrevLink = temp;
	else
		CurrPos++;
}

void TDatList::InsLast(PTDatValue pVal) // �������� ���������
{
	PTDatLink temp = GetLink(pVal, pStop);

	if (pLast != nullptr)
		pLast->SetNextLink(temp);
		
	pLast = temp;
	ListLen++;

	if (ListLen == 1)
	{
		pFirst = temp;
		Reset();
	}

	if (IsListEnded())
		CurrPos++;
}

void TDatList::InsCurrent(PTDatValue pVal) // ����� �������
{
	if (IsEmpty() || pCurrLink == pFirst)
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else
	{
		PTDatLink temp = GetLink(pVal, pCurrLink);
		pPrevLink->SetNextLink(temp);
		pCurrLink = temp;
		ListLen++;
	}
}

void TDatList::DelFirst()
{
	if (!IsEmpty())
	{
		PTDatLink temp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(temp);
		if (IsEmpty())
		{
			pLast = pStop;
			Reset();
		}
		else if (CurrPos == 0)
			pCurrLink = pFirst;
		else if (CurrPos == 1)
			pPrevLink = pStop;
		if (CurrPos > 0)
			CurrPos--;
	}
}

void TDatList::DelCurrent()
{
	if (pCurrLink != pStop)
	{
		if (pCurrLink == pFirst)
			DelFirst();
		else
		{
			PTDatLink temp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			DelLink(temp);
			if (pCurrLink == pLast)
			{
				pLast = pPrevLink;
				pCurrLink = pStop;
			}
		}
	}
}

void TDatList::DelList()
{
	while (!IsEmpty())
		DelFirst();
	pFirst = pLast = pPrevLink = pCurrLink = pStop;
	CurrPos = -1;
}
