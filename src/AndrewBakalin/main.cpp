#include "Polinom.h"

int main()
{
	int monoms1[][2] = { { 5, 701 },{ -2, 490 },{ 4, 207 },{ 6, 027 } };
	int monoms2[][2] = { { 2, 490 },{ 7, 320 } };
	TPolinom p1(monoms1, 4);
	TPolinom p2(monoms2, 2);
	cout << "First polinom:" << '\n' << p1 << endl;
	cout << "Second polinom:" << '\n' << p2 << endl;

	p1 + p2;
	cout << "Sum:" << endl << p1 << endl;

	return 0;
}