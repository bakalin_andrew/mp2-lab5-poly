#include "Polinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	
	for (int i = 0; i < km; i++)
	{
		monom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(monom);
	}
}

TPolinom::TPolinom(TPolinom &q)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		monom = (PTMonom)q.GetDatValue();
		InsLast(monom->GetCopy());
	}

	Reset();
}

TPolinom& TPolinom::operator+(TPolinom &q)
{
	PTMonom curM, qM, newM;
	q.Reset();
	Reset();

	while (true) {
		curM = GetMonom();
		qM = q.GetMonom();
		if (curM->Index < qM->Index) {
			newM = new TMonom(qM->Coeff, qM->Index);
			InsCurrent(newM);
			q.GoNext();
		}
		else if (curM->Index > qM->Index) {
			GoNext();
		}
		else {
			if (curM->Index == -1)
				break;
			curM->Coeff += qm->Coeff;
			if (curM->Coeff != 0) {
				GoNext();
				q.GoNext();
			}
			else {
				DelCurrent();
				q.GoNext();
			}
		}
	}
	return *this;
}


TPolinom & TPolinom::operator=(TPolinom &q)
{
	DelList();

	if (&q == nullptr)
		throw -1;

	if (&q != this)
	{
		PTMonom Mon = new TMonom(0, -1);
		pHead->SetDatValue(Mon);
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			Mon = q.GetMonom();
			InsLast(Mon->GetCopy());
		}
	}
	return *this;
}

double TPolinom::Calculate(int _x, int _y, int _z)
{
	PTMonom monom;
	double result = 0;
	int x, y, z;
	if (ListLen)
	{
		for (Reset(); !IsListEnded(); GoNext())
		{
			monom = GetMonom();
			x = monom->GetIndex() / 100;
			y = (monom->GetIndex() % 100) / 10;
			z = monom->GetIndex() % 10;
			result += monom->GetCoeff() * pow(_x, x) * pow(_y, y) * pow(_z, z);
		}
	}
	return result;
}

bool TPolinom:: operator==(TPolinom &q)
{
	bool isEqual = true;
	if (q.GetListLength() == GetListLength())
	{
		for (q.Reset(), Reset(); !q.IsListEnded(); q.GoNext(), GoNext())
			if (!(*(PTMonom)q.GetDatValue() == *(PTMonom)GetDatValue()))
			{
				isEqual = false;
				break;
			}
	}
	else
		isEqual = false;

	return isEqual;
}

ostream& operator<<(ostream &os, TPolinom &q)
{
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
		cout << *q.GetMonom() << endl;
	return os;
}