# Методы программирования 2: Полиномы

## Введение

### Цели и задачи

В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:

-	ввод полинома 
-	организация хранения полинома
-	удаление введенного ранее полинома;
-	копирование полинома;
-	сложение двух полиномов;
-	вычисление значения полинома при заданных значениях переменных;
-	вывод.

Состав реализуемых операций над полиномами может быть расширен при постановке задания лабораторной работы.
Предполагается, что в качестве структуры хранения будут использоваться списки. В качестве дополнительной цели в лабораторной работе ставится также задача разработки некоторого общего представления списков и операций по их обработке. В числе операций над списками должны быть реализованы следующие действия:

-	поддержка понятия текущего звена;
-	вставка звеньев в начало, после текущей позиции и в конец списков;
-	удаление звеньев в начале и в текущей позиции списков;
-	организация последовательного доступа к звеньям списка (итератор).

### Условия и ограничения

При выполнении лабораторной работы можно использовать следующие основные предположения:

1.	Разработка структуры хранения должна быть ориентирована на представление полиномов от трех неизвестных.
2.	Степени переменных полиномов не могут превышать значения 9, т.е. 0 <= i, j, k <= 9.
3.	Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).

### Структура классов

![](/src/AndrewBakalin/images/struct.png)

## Реализация классов

###Абстрактный класс объектов-значений

*DatValue.h*

	#pragma once

	class TDatValue {
	public:
		virtual TDatValue * GetCopy() = 0; // создание копии
		~TDatValue() {}
	};

	typedef TDatValue* PTDatValue;
	
###Базовый класс для звеньев
*RootLink.h*

	#pragma once
	#include "DatValue.h"

	class TRootLink;
	typedef TRootLink* PTRootLink;

	class TRootLink {
	protected:
		PTRootLink  pNext;  // указатель на следующее звено
	public:
		TRootLink(PTRootLink pN = nullptr) { pNext = pN; }
		PTRootLink  GetNextLink() { return  pNext; }
		void SetNextLink(PTRootLink  pLink) { pNext = pLink; }
		void InsNextLink(PTRootLink  pLink) {
			PTRootLink p = pNext;  pNext = pLink;
			if (pLink != nullptr) pLink->pNext = p;
		}
		virtual void       SetDatValue(PTDatValue pVal) = 0;
		virtual PTDatValue GetDatValue() = 0;

		friend class TDatList;
	};
	
###Звено с указателем на объект-значение
*DatLink.h*

	#pragma once
	#include "RootLink.h"

	class TDatLink;
	typedef TDatLink* PTDatLink;

	class TDatLink : public TRootLink {
	protected:
		PTDatValue pValue;  // указатель на объект значения
	public:
		TDatLink(PTDatValue pVal = nullptr, PTRootLink pN = nullptr) :
			TRootLink(pN) {
			pValue = pVal;
		}
		void       SetDatValue(PTDatValue pVal) { pValue = pVal; }
		PTDatValue GetDatValue() { return  pValue; }
		PTDatLink  GetNextDatLink() { return  (PTDatLink)pNext; }
		friend class TDatList;
	};
	
###Класс линейных списков
*DatList.h*

	#pragma once
	#include "DatLink.h"

	enum TLinkPos {FIRST, CURRENT, LAST};

	class TDatList {
	protected:
		PTDatLink pFirst;    // первое звено
		PTDatLink pLast;     // последнее звено
		PTDatLink pCurrLink; // текущее звено
		PTDatLink pPrevLink; // звено перед текущим
		PTDatLink pStop;     // значение указателя, означающего конец списка 
		int CurrPos;         // номер текущего звена (нумерация от 0)
		int ListLen;         // количество звеньев в списке
	protected:  // методы
		PTDatLink GetLink(PTDatValue pVal = nullptr, PTDatLink pLink = nullptr); // выделение звена
		void      DelLink(PTDatLink pLink);   // удаление звена
	public:
		TDatList();
		~TDatList() { DelList(); }
		// доступ
		PTDatValue GetDatValue(TLinkPos mode = CURRENT) const; // значение
		virtual int IsEmpty()  const { return pFirst == pStop; } // список пуст ?
		int GetListLength()    const { return ListLen; }       // к-во звеньев
															   // навигация
		void SetCurrentPos(int pos);      // установить текущее звено
		int GetCurrentPos() const;       // получить номер тек. звена
		virtual void Reset();             // установить на начало списка
		virtual int IsListEnded() const; // список завершен ?
		void GoNext();                    // сдвиг вправо текущего звена
											 // (=1 после применения GoNext для последнего звена списка)
											 // вставка звеньев
		virtual void InsFirst(PTDatValue pVal = nullptr); // перед первым
		virtual void InsLast(PTDatValue pVal = nullptr); // вставить последним 
		virtual void InsCurrent(PTDatValue pVal = nullptr); // перед текущим 
														 // удаление звеньев
		virtual void DelFirst();    // удалить первое звено 
		virtual void DelCurrent();    // удалить текущее звено 
		virtual void DelList();    // удалить весь список
	};
	
*DatList.cpp*

	#include "DatList.h"

	PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink) // выделение звена
	{
		return new TDatLink(pVal, pLink);
	}

	void TDatList::DelLink(PTDatLink pLink) // удаление звена
	{
		delete pLink->pValue;
		delete pLink;
	}

	PTDatValue TDatList::GetDatValue(TLinkPos mode) const // значение
	{
		PTDatLink temp;

		switch (mode)
		{
		case FIRST:
			temp = pFirst;
			break;
		case LAST:
			temp = pLast;
			break;
		default:
			temp = pCurrLink;
		}

		if (temp == nullptr)
			return nullptr;
		else
			return temp->pValue;
	}

	TDatList::TDatList()
	{
		pFirst = pLast = pStop = pPrevLink = pCurrLink = nullptr;
		ListLen = 0;
		Reset();
	}

	void TDatList::SetCurrentPos(int pos) // установить текущее звено
	{
		if (pos >= ListLen)
			throw 1;
		else
		{
			Reset();
			for (int i = 0; i < pos; i++)
				GoNext();
		}
	}

	int TDatList::GetCurrentPos() const // получить номер тек. звена
	{
		return CurrPos;
	}

	void TDatList::Reset() // установить на начало списка
	{
		if (IsEmpty())
		{
			pCurrLink = pStop;
			CurrPos = -1;
		}
		else
		{
			pCurrLink = pFirst;
			CurrPos = 0;
		}
	}

	int TDatList::IsListEnded() const // список завершен ?
	{
		return pCurrLink == pStop;
	}

	void TDatList::GoNext() // сдвиг вправо текущего звена
	{
		if (pCurrLink != pStop)
		{
			pPrevLink = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			CurrPos++;
		}
	}

	// вставка звеньев

	void TDatList::InsFirst(PTDatValue pVal) // перед первым
	{
		PTDatLink temp = GetLink(pVal, pFirst);

		pFirst = temp;
		ListLen++;

		if (ListLen == 1)
		{
			pLast = temp;
			Reset();
		}

		if (CurrPos == 0)
			pPrevLink = temp;
		else
			CurrPos++;
	}

	void TDatList::InsLast(PTDatValue pVal) // вставить последним
	{
		PTDatLink temp = GetLink(pVal, pStop);

		if (pLast != nullptr)
			pLast->SetNextLink(temp);

		pLast = temp;
		ListLen++;

		if (ListLen == 1)
		{
			pFirst = temp;
			Reset();
		}

		if (IsListEnded())
			CurrPos++;
	}

	void TDatList::InsCurrent(PTDatValue pVal) // перед текущим
	{
		if (IsEmpty() || pCurrLink == pFirst)
			InsFirst(pVal);
		else if (IsListEnded())
			InsLast(pVal);
		else
		{
			PTDatLink temp = GetLink(pVal, pCurrLink);
			pPrevLink->SetNextLink(temp);
			pCurrLink = temp;
			ListLen++;
		}
	}

	void TDatList::DelFirst()
	{
		if (!IsEmpty())
		{
			PTDatLink temp = pFirst;
			pFirst = pFirst->GetNextDatLink();
			DelLink(temp);
			if (IsEmpty())
			{
				pLast = pStop;
				Reset();
			}
			else if (CurrPos == 0)
				pCurrLink = pFirst;
			else if (CurrPos == 1)
				pPrevLink = pStop;
			if (CurrPos > 0)
				CurrPos--;
		}
	}

	void TDatList::DelCurrent()
	{
		if (pCurrLink != pStop)
		{
			if (pCurrLink == pFirst)
				DelFirst();
			else
			{
				PTDatLink temp = pCurrLink;
				pCurrLink = pCurrLink->GetNextDatLink();
				pPrevLink->SetNextLink(pCurrLink);
				DelLink(temp);
				if (pCurrLink == pLast)
				{
					pLast = pPrevLink;
					pCurrLink = pStop;
				}
			}
		}
	}

	void TDatList::DelList()
	{
		while (!IsEmpty())
			DelFirst();
		pFirst = pLast = pPrevLink = pCurrLink = pStop;
		CurrPos = -1;
	}
	
###Циклические списки с заголовком
*HeadRing.h*

	#pragma once
	#include "DatList.h"

	class THeadRing : public TDatList {
	protected:
		PTDatLink pHead;     // заголовок, pFirst - звено за pHead
	public:
		THeadRing();
		~THeadRing();
		// вставка звеньев
		virtual void InsFirst(PTDatValue pVal = nullptr); // после заголовка
		// удаление звеньев
		virtual void DelFirst(void);                 // удалить первое звено
	};
	
*HeadRing.cpp*

	#include "HeadRing.h"

	THeadRing::THeadRing() : TDatList()
	{
		TDatList::InsFirst();
		pHead = pFirst;
		pStop = pHead;
		Reset();
		ListLen = 0;
		pHead->SetNextLink(pHead);
	}

	THeadRing:: ~THeadRing()
	{
		DelList();
		DelLink(pHead);
		pHead = nullptr;
	}

	void THeadRing::InsFirst(PTDatValue pVal)
	{
		TDatList::InsFirst(pVal);
		pHead->SetNextLink(pFirst);
	}

	void THeadRing::DelFirst()
	{
		TDatList::DelFirst();
		pHead->SetNextLink(pFirst);
	}
	
###Класс мономов
*Monom.h*

	#pragma once
	#include <iostream>
	#include "DatValue.h"

	using namespace std;

	class TMonom : public TDatValue {
	protected:
		int Coeff; // коэффициент монома
		int Index; // индекс (свертка степеней)
	public:
		TMonom(int cval = 1, int ival = 0);
		virtual TDatValue * GetCopy(); // изготовить копию
		void SetCoeff(int cval);
		int  GetCoeff();
		void SetIndex(int ival);
		int  GetIndex();
		TMonom& operator=(const TMonom &tm);
		int operator==(const TMonom &tm);
		int operator<(const TMonom &tm);
		friend ostream& operator<<(ostream &os, TMonom &tm);
		friend class TPolinom;
	};

	typedef TMonom* PTMonom;
	
*Monom.cpp*

	#include "Monom.h"

	TMonom::TMonom(int cval, int ival)
	{
		Coeff = cval; Index = ival;
	}

	TDatValue* TMonom::GetCopy()
	{
		return new TMonom(Coeff, Index);
	}

	void TMonom::SetCoeff(int cval)
	{
		Coeff = cval;
	}

	int TMonom::GetCoeff()
	{
		return Coeff;
	}

	void TMonom::SetIndex(int ival)
	{
		Index = ival;
	}

	int TMonom::GetIndex()
	{
		return Index;
	}

	TMonom& TMonom::operator=(const TMonom &tm)
	{
		Coeff = tm.Coeff; Index = tm.Index;
		return *this;
	}

	int TMonom::operator==(const TMonom &tm) {
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}

	int TMonom::operator<(const TMonom &tm) {
		return Index<tm.Index;
	}

	ostream& operator<<(ostream &os, TMonom &tm) {
		os << tm.Coeff << " " << tm.Index;
		return os;
	}
	
###Класс полиномов
*Polinom.h*

	#pragma once
	#include "HeadRing.h"
	#include "Monom.h"

	class TPolinom : public THeadRing {
	public:
		TPolinom(int monoms[][2] = nullptr, int km = 0); // конструктор
													  // полинома из массива «коэффициент-индекс»
		TPolinom(TPolinom &q);      // конструктор копирования
		PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
		TPolinom & operator+(TPolinom &q); // сложение полиномов
		TPolinom & operator=(TPolinom &q); // присваивание
		bool operator==(TPolinom &q); // сравнение
		double Calculate(int x, int y, int z); // вычисление значения полинома
		friend ostream& operator<<(ostream &os, TPolinom &q); // вывод
	};
	
*Polinom.cpp*

	#include "Polinom.h"

	TPolinom::TPolinom(int monoms[][2], int km)
	{
		PTMonom monom = new TMonom(0, -1);
		pHead->SetDatValue(monom);

		for (int i = 0; i < km; i++)
		{
			monom = new TMonom(monoms[i][0], monoms[i][1]);
			InsLast(monom);
		}
	}

	TPolinom::TPolinom(TPolinom &q)
	{
		PTMonom monom = new TMonom(0, -1);
		pHead->SetDatValue(monom);

		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			monom = (PTMonom)q.GetDatValue();
			InsLast(monom->GetCopy());
		}

		Reset();
	}

	TPolinom& TPolinom::operator+(TPolinom &q)
	{
		PTMonom pm, qm, rm;
		q.Reset();
		Reset();

		while (true) {
			pm = GetMonom();
			qm = q.GetMonom();
			if (pm->Index < qm->Index) {
				rm = new TMonom(qm->Coeff, qm->Index);
				InsCurrent(rm);
				q.GoNext();
			}
			else if (pm->Index > qm->Index) {
				GoNext();
			}
			else {
				if (pm->Index == -1)
					break;
				pm->Coeff += qm->Coeff;
				if (pm->Coeff != 0) {
					GoNext();
					q.GoNext();
				}
				else {
					DelCurrent();
					q.GoNext();
				}
			}
		}
		return *this;
	}


	TPolinom & TPolinom::operator=(TPolinom &q)
	{
		DelList();

		if (&q == nullptr)
			throw -1;

		if (&q != this)
		{
			PTMonom Mon = new TMonom(0, -1);
			pHead->SetDatValue(Mon);
			for (q.Reset(); !q.IsListEnded(); q.GoNext())
			{
				Mon = q.GetMonom();
				InsLast(Mon->GetCopy());
			}
		}
		return *this;
	}

	double TPolinom::Calculate(int _x, int _y, int _z)
	{
		PTMonom monom;
		double result = 0;
		int x, y, z;
		if (ListLen)
		{
			for (Reset(); !IsListEnded(); GoNext())
			{
				monom = GetMonom();
				x = monom->GetIndex() / 100;
				y = (monom->GetIndex() % 100) / 10;
				z = monom->GetIndex() % 10;
				result += monom->GetCoeff() * pow(_x, x) * pow(_y, y) * pow(_z, z);
			}
		}
		return result;
	}

	bool TPolinom:: operator==(TPolinom &q)
	{
		bool isEqual = true;
		if (q.GetListLength() == GetListLength())
		{
			for (q.Reset(), Reset(); !q.IsListEnded(); q.GoNext(), GoNext())
				if (!(*(PTMonom)q.GetDatValue() == *(PTMonom)GetDatValue()))
				{
					isEqual = false;
					break;
				}
		}
		else
			isEqual = false;

		return isEqual;
	}

	ostream& operator<<(ostream &os, TPolinom &q)
	{
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
			cout << *q.GetMonom() << endl;
		return os;
	}
	
###Тестирование класса Полином
*test_polinom.cpp*

	TEST(TPolinom, Can_Make_Empty_Polinom)
	{
		ASSERT_NO_THROW(TPolinom p);
	}

	TEST(TPolinom, Can_Make_Polinom_With_Arguments)
	{
		int monoms[][2] = { { 5, 701 },{ 4, 207 } };

		ASSERT_NO_THROW(TPolinom p(monoms, 2));
	}

	TEST(TPolinom, Can_Copy_Polinom)
	{
		int monoms[][2] = { { 5, 701 },{ 4, 207 } };
		TPolinom p1(monoms, 2);

		ASSERT_NO_THROW(TPolinom p2(p1));
	}

	TEST(TPolinom, Copied_Polinom_Is_Equal_To_Itself)
	{
		int monoms[][2] = { { 5, 701 },{ 4, 207 } };
		TPolinom p1(monoms, 2), p2(p1);

		EXPECT_EQ(true, p1 == p2);
	}

	TEST(TPolinom, Comparison_Returns_True_With_Equal_Polinoms)
	{
		int monoms[][2] = { { 5, 701 },{ 4, 207 } };
		TPolinom p1(monoms, 2), p2(monoms, 2);

		EXPECT_EQ(true, p1 == p2);
	}

	TEST(TPolinom, Comparison_Returns_False_With_Different_Polinoms)
	{
		int monoms1[][2] = { { 5, 701 },{ 4, 207 } };
		int monoms2[][2] = { { 6, 535 },{ 4, 207 } };
		TPolinom p1(monoms1, 2), p2(monoms2, 2);

		EXPECT_EQ(false, p1 == p2);
	}

	TEST(TPolinom, GetMonom_Works_Correctly)
	{
		int monoms[][2] = { { 5, 701 } };
		TPolinom p(monoms, 1);
		TMonom m(5, 701);

		p.Reset();
		TMonom expected = *p.GetMonom();

		EXPECT_EQ(true, m == expected);
	}

	TEST(TPolinom, Can_Assign_Polinoms)
	{
		int monoms[][2] = { { 5, 701 },{ 4, 207 } };
		TPolinom p1(monoms, 2), p2;

		ASSERT_NO_THROW(p2 = p1);
	}

	TEST(TPolinom, Can_Assign_Polinom_To_Itself)
	{
		int monoms[][2] = { { 5, 701 },{ 4, 207 } };
		TPolinom p(monoms, 2);

		ASSERT_NO_THROW(p = p);
	}

	TEST(TPolinom, Assign_Works_Correctly)
	{
		int monoms[][2] = { { 5, 701 },{ 4, 207 } };
		TPolinom p1(monoms, 2), p2;

		p2 = p1;

		EXPECT_EQ(true, p1 == p2);
	}

	TEST(TPolinom, Can_Add_Two_Polinoms)
	{
		int monoms1[][2] = { { 5, 701 },{ 4, 207 } };
		int monoms2[][2] = { { -2, 490 },{ 6, 027 } };
		TPolinom p1(monoms1, 2), p2(monoms2, 2);

		ASSERT_NO_THROW(p1 + p2);
	}

	TEST(TPolinom, Addition_Works_Correctly)
	{
		int monoms1[][2] = { { 5, 701 },{ 4, 207 } };
		int monoms2[][2] = { { -2, 490 },{ 6, 027 } };
		int monoms3[][2] = { { 5, 701 },{ -2, 490 },{ 4, 207 },{ 6, 027 } };
		TPolinom p1(monoms1, 2), p2(monoms2, 2), p3(monoms3, 4);

		p1 + p2;

		EXPECT_EQ(true, p3 == p1);
	}

	TEST(TPolinom, Calculate_Works_Correctly)
	{
		int monoms[][2] = { { 5, 321 },{ -2, 301 } };
		TPolinom p(monoms, 2);

		EXPECT_EQ(2952, p.Calculate(2, 5, 3));
	}

*Результат работы*
![](/src/AndrewBakalin/images/tests.png)

###Простой пример использования
*main.cpp*
	#include "Polinom.h"

	int main()
	{
		int monoms1[][2] = { { 5, 701 },{ -2, 490 },{ 4, 207 },{ 6, 027 } };
		int monoms2[][2] = { { 2, 490 },{ 7, 320 } };
		TPolinom p1(monoms1, 4);
		TPolinom p2(monoms2, 2);
		cout << "First polinom:" << '\n' << p1 << endl;
		cout << "Second polinom:" << '\n' << p2 << endl;

		p1 + p2;
		cout << "Sum:" << endl << p1 << endl;

		return 0;
	}

*Результат работы*
![](/src/AndrewBakalin/images/test.png)

##Вывод
В ходе лабораторной работы были получены навыки работы с очень важной структурой данных - списками. На основе разработанных списков был реализован важный математический объект - полином, который был протестирован с помощью фреймворка Google Test.  