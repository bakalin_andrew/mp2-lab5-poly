#include "Monom.h"

TMonom::TMonom(int cval, int ival)
{
	Coeff = cval; Index = ival;
}

TDatValue* TMonom::GetCopy()
{
	return new TMonom(Coeff, Index);
}

void TMonom::SetCoeff(int cval)
{
	Coeff = cval;
}

int TMonom::GetCoeff()
{
	return Coeff;
}

void TMonom::SetIndex(int ival)
{
	Index = ival;
}

int TMonom::GetIndex()
{
	return Index;
}

TMonom& TMonom::operator=(const TMonom &tm)
{
	Coeff = tm.Coeff; Index = tm.Index;
	return *this;
}

int TMonom::operator==(const TMonom &tm) {
	return (Coeff == tm.Coeff) && (Index == tm.Index);
}

int TMonom::operator<(const TMonom &tm) {
	return Index<tm.Index;
}

ostream& operator<<(ostream &os, TMonom &tm) {
	os << tm.Coeff << " " << tm.Index;
	return os;
}
