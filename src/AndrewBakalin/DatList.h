#pragma once
#include "DatLink.h"

enum TLinkPos {FIRST, CURRENT, LAST};

class TDatList {
protected:
	PTDatLink pFirst;    // ������ �����
	PTDatLink pLast;     // ��������� �����
	PTDatLink pCurrLink; // ������� �����
	PTDatLink pPrevLink; // ����� ����� �������
	PTDatLink pStop;     // �������� ���������, ����������� ����� ������ 
	int CurrPos;         // ����� �������� ����� (��������� �� 0)
	int ListLen;         // ���������� ������� � ������
protected:  // ������
	PTDatLink GetLink(PTDatValue pVal = nullptr, PTDatLink pLink = nullptr); // ��������� �����
	void      DelLink(PTDatLink pLink);   // �������� �����
public:
	TDatList();
	~TDatList() { DelList(); }
	// ������
	PTDatValue GetDatValue(TLinkPos mode = CURRENT) const; // ��������
	virtual int IsEmpty()  const { return pFirst == pStop; } // ������ ���� ?
	int GetListLength()    const { return ListLen; }       // �-�� �������
														   // ���������
	void SetCurrentPos(int pos);      // ���������� ������� �����
	int GetCurrentPos() const;       // �������� ����� ���. �����
	virtual void Reset();             // ���������� �� ������ ������
	virtual int IsListEnded() const; // ������ �������� ?
	void GoNext();                    // ����� ������ �������� �����
										 // (=1 ����� ���������� GoNext ��� ���������� ����� ������)
										 // ������� �������
	virtual void InsFirst(PTDatValue pVal = nullptr); // ����� ������
	virtual void InsLast(PTDatValue pVal = nullptr); // �������� ��������� 
	virtual void InsCurrent(PTDatValue pVal = nullptr); // ����� ������� 
													 // �������� �������
	virtual void DelFirst();    // ������� ������ ����� 
	virtual void DelCurrent();    // ������� ������� ����� 
	virtual void DelList();    // ������� ���� ������
};
