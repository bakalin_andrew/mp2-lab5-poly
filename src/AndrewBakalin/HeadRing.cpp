#include "HeadRing.h"

THeadRing::THeadRing() : TDatList()
{
	TDatList::InsFirst();
	pHead = pFirst;
	pStop = pHead;
	Reset();
	ListLen = 0;
	pHead->SetNextLink(pHead);
}

THeadRing:: ~THeadRing()
{
	DelList();
	DelLink(pHead);
	pHead = nullptr;
}

void THeadRing::InsFirst(PTDatValue pVal)
{
	TDatList::InsFirst(pVal);
	pHead->SetNextLink(pFirst);
}

void THeadRing::DelFirst()
{
	TDatList::DelFirst();
	pHead->SetNextLink(pFirst);
}