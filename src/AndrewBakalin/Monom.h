#pragma once
#include <iostream>
#include "DatValue.h"

using namespace std;

class TMonom : public TDatValue {
protected:
	int Coeff; // ����������� ������
	int Index; // ������ (������� ��������)
public:
	TMonom(int cval = 1, int ival = 0);
	virtual TDatValue * GetCopy(); // ���������� �����
	void SetCoeff(int cval);
	int  GetCoeff();
	void SetIndex(int ival);
	int  GetIndex();
	TMonom& operator=(const TMonom &tm);
	int operator==(const TMonom &tm);
	int operator<(const TMonom &tm);
	friend ostream& operator<<(ostream &os, TMonom &tm);
	friend class TPolinom;
};

typedef TMonom* PTMonom;